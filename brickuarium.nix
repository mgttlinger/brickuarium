{ haskellPackages, lib }:

let
  brickuarium = haskellPackages.callCabal2nix "brickuarium" (lib.cleanSource ./.) {};
in
brickuarium
