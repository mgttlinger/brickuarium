{ nixpkgs ? import <nixos> {}, compiler ? "ghc882" }:

let normalHaskellPackages = nixpkgs.pkgsMusl.haskell.packages.${compiler};

    haskellPackages = with nixpkgs.pkgsMusl.haskell.lib; normalHaskellPackages.override {
      overrides = self: super: {
        # Dependencies we need to patch
        #hpc-coveralls = appendPatch super.hpc-coveralls (builtins.fetchurl https://github.com/guillaume-nargeot/hpc-coveralls/pull/73/commits/344217f513b7adfb9037f73026f5d928be98d07f.patch);
        hslua = dontCheck normalHaskellPackages.hslua;
      };
    };
in
rec {
  # brickuariumStatic = (haskellPackages.callPackage ./brickuarium.nix {}).overrideAttrs (old: { configureFlags = [
  #   "--ghc-option=-optl=-static"
  #   "--extra-lib-dirs=${nixpkgs.pkgsMusl.gmp6.override { withStatic = true; }}/lib"
  #   "--extra-lib-dirs=${nixpkgs.pkgsMusl.zlib.static}/lib"
  #   "--extra-lib-dirs=${nixpkgs.pkgsMusl.libffi.overrideAttrs (old: { dontDisableStatic = true; })}/lib"
  # ];});
  brickuarium = nixpkgs.haskell.packages.${compiler}.callPackage ./brickuarium.nix {};
  # brickuariumDocker = nixpkgs.pkgs.callPackage ./docker.nix { inherit brickuarium; };
}
