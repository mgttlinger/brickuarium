{ pkgs, brickuarium }:

pkgs.dockerTools.buildImage {
  name = "brickuariumDocker";
  tag = "latest";

  fromImageName = "alpine";
  fromImageTag = "latest";

  contents = brickuarium;
  runAsRoot = ''
    #!${pkgs.runtimeShell}
    mkdir -p /data
  '';

  config = {
    Entrypoint = [ "/bin/brickuarium" ];
    WorkingDir = "/data";
    Volumes = {
      "/data" = {};
    };
  };
}
