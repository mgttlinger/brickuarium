{-# LANGUAGE RecordWildCards #-}
module Main where

import Prelude hiding (last)

import Brick
import Brick.BChan
import Brick.Widgets.Border
import Control.Concurrent
import Control.Monad
import Data.List
import Data.Void
import Graphics.Vty

data EntityType = Fish { fishTail :: String, fishBody :: String, fishEye :: String, fishMouth :: String }
                | Plant { plantStem :: String, plantHead :: String, plantLength :: Int }
  deriving (Show, Eq)

drawEntityType :: EntityType -> Widget Void
drawEntityType Fish{..} = hBox [ str fishTail, str fishBody, str fishEye, str fishMouth ]
drawEntityType Plant{..} = fill ' ' <=> (vBox $ (str plantHead) : (take plantLength $ repeat $ str plantStem))

standardFish :: EntityType
standardFish = Fish { fishTail = "><"
                    , fishBody = "((("
                    , fishEye = "°"
                    , fishMouth = ">"
                    }

plant :: Int -> EntityType
plant l = Plant { plantHead = "+"
                , plantStem = "|"
                , plantLength = l
                }


data Entity = MkEntity { tpe :: EntityType
                       , entityX :: Int
                       , entityY :: Int
                       , entityWidth :: Int
                       }
  deriving (Show, Eq)

drawEntity :: Entity -> Int -> Widget Void
drawEntity MkEntity{..} x = padLeft (Pad $ entityX - x) $ drawEntityType tpe

rowList :: Int -> [Entity] -> [Widget Void]
rowList _ [] = []
rowList y es = let (here, there) = partition ((y ==) . entityY) es
                   ordered = sortOn entityX here
               in rowList (y + 1) there <> [ border $ padRight Max $ fst (foldl (\ (w,x) e@MkEntity{..} ->  (w <+> (drawEntity e x), entityX + entityWidth)) (emptyWidget, 0) ordered) ]

drawEntities :: [Entity] -> Widget Void
drawEntities es = vBox $ rowList 0 es

data BrickuariumState = MkBrickuariumState { entities :: [Entity] }

brickuariumEventHandler :: BrickuariumState -> BrickEvent Void () -> EventM Void (Next BrickuariumState)
brickuariumEventHandler MkBrickuariumState{..} (AppEvent ()) =  continue $ MkBrickuariumState entities
brickuariumEventHandler s l = resizeOrQuit s l

brickuariumApp :: App BrickuariumState () Void
brickuariumApp = App { appDraw = \ MkBrickuariumState{..} ->
                         [border $ padBottom Max $ drawEntities entities]
                     , appChooseCursor = neverShowCursor
                     , appHandleEvent = brickuariumEventHandler
                     , appStartEvent = \ left -> pure left
                     , appAttrMap = const $ attrMap defAttr []
                     }

brickuarium :: IO BrickuariumState
brickuarium = do
  eventChan <- newBChan 10
  let buildVty = mkVty defaultConfig
  initialVty <- buildVty
  void . forkIO $ forever $ do
    writeBChan eventChan ()
    threadDelay 100000
  customMain initialVty buildVty (Just eventChan) brickuariumApp $ MkBrickuariumState [ MkEntity standardFish 0 0 7, MkEntity standardFish 1 0 7 ]


main :: IO ()
main = void brickuarium
